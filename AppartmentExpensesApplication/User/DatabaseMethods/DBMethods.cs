﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using User.Models;

namespace User.DatabaseMethods
{
    public class DBMethods
    {
        static string str = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        public void AddUser(SignUp model)
        {
            using(SqlConnection conn = new SqlConnection(str))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("AddUser", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Username", model.Username);
                cmd.Parameters.AddWithValue("@Password", model.Password);
                cmd.Parameters.AddWithValue("@Email", model.Email);
                cmd.Parameters.AddWithValue("@FullName", model.FullName);
                cmd.Parameters.AddWithValue("@CreatedAt", DateTime.Now);
                cmd.Parameters.AddWithValue("@UpdateAt", DateTime.Now);
                cmd.ExecuteNonQuery();
            }
        }

        public bool CheckIfLoginExists(LogIn model)
        {
            bool exists = false;
            using(SqlConnection conn = new SqlConnection(str))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("SelectUser", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Email", model.Email);
                SqlDataReader reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        if (reader["Password"].ToString() == model.Password)
                        {
                            exists = true;
                        }
                    }
                    
                }
                reader.Close();
            }
            return exists;
        }

        public bool CheckIfSignUpExists(SignUp model)
        {
            bool exists = false;
            using (SqlConnection conn = new SqlConnection(str))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("SelectUser", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Email", model.Email);
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    exists = true;
                }
                reader.Close();
            }
            return exists;
        }

        public static List<ExpensePage> GetExpensePages()
        {
            List<ExpensePage> list = new List<ExpensePage>();
            using (SqlConnection conn = new SqlConnection(str))
            {
                SqlCommand cmd = new SqlCommand("GetExpenses", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new ExpensePage()
                        {
                            ExpenseDate = Convert.ToDateTime(reader["ExpenseDate"]),
                            ExpenseType = reader["ExpenseType"].ToString(),
                            ExpenseAmount = Convert.ToDecimal(reader["ExpenseAmount"]),
                            Description = reader["Description"].ToString()
                        });
                    }
                }
            }
            return list;
        }
    }
}