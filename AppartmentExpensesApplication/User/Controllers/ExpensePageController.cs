﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using User.DatabaseMethods;

namespace User.Controllers
{
    [Authorize]
    public class ExpensePageController : Controller
    {
        DBMethods db = new DBMethods();
        // GET: ExpensePage
        public ActionResult Index()
        {
            return View(DBMethods.GetExpensePages());
        }
    }
}