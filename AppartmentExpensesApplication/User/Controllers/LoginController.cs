﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using User.DatabaseMethods;
using User.Models;

namespace User.Controllers
{
    public class LoginController : Controller
    {
        DBMethods db = new DBMethods();
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(LogIn model)
        {
            if (db.CheckIfLoginExists(model))
            {
                FormsAuthentication.SetAuthCookie(model.Email,false);
                return RedirectToAction("Index","Home");
            }
            return View();
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index","Login");
        }

    }
}