﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using User.DatabaseMethods;
using User.Models;

namespace User.Controllers
{
    public class SignUpController : Controller
    {
        DBMethods db = new DBMethods();
        // GET: SignUp
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(SignUp model)
        {
            if (!db.CheckIfSignUpExists(model))
            {
                db.AddUser(model);
            }
            return View();
        }
    }
}